<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class ProviderController extends Controller
{
    public function redirect($provider)
    {
        if($provider == 'github') {
            return Socialite::driver('github')
                ->scopes(['read:user', 'public_repo'])
                ->redirect();
        }
        
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {

        if($provider == 'github') {
            $githubUser = Socialite::driver($provider)->user();
            $repositories = $githubUser->user['public_repos'];
            dd($repositories);

            return;
        }

        $socialUser = Socialite::driver($provider)->user();
 
        $user = User::updateOrCreate([
            'provider_id' => $socialUser->id,
            'provider' => $provider
        ], [
            'name' => $socialUser->name,
            'email' => $socialUser->email,
            'provider_token' => $socialUser->token
        ]);
    
        Auth::login($user);
    
        return redirect('/dashboard');
    }
}
